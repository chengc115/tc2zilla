// #include "LMotorController.h"
#include "Arduino.h"

#ifdef ESP32
#define analogWrite(p,v) ledcWrite(p,v)
// channels 0-15, resolution 1-16 bits, freq limits depend on resolution
// channels 0-15, resolution 1-16 bits, freq limits depend on resolution
#define PWMFreq 12000  // 12KHZ
#define RESOLUTION_BITS 8

#define AChannel 3
#define BChannel 2

#define max(x,y) ((x)>(y)?x:y)
#endif


class LMotorController {
  private:
    uint8_t _ena, _in1, _in2, _in3, _in4, _enb;
    double _motorAConst, _motorBConst;
    int16_t _currentSpeed;

  public:
    LMotorController(int ena, int in1, int in2, int in3, int in4, int enb,
                     double motorAConst, double motorBConst) {
      _motorAConst = motorAConst;
      _motorBConst = motorBConst;
      _ena = ena;
      _enb = enb;
      _in1 = in2;
      _in2 = in1;
      _in3 = in3;
      _in4 = in4;

      pinMode(_in1, OUTPUT);
      pinMode(_in2, OUTPUT);

      pinMode(_in3, OUTPUT);
      pinMode(_in4, OUTPUT);
      digitalWrite(_in1, LOW);
      digitalWrite(_in2, LOW);
      digitalWrite(_in3, LOW);
      digitalWrite(_in4, LOW);

#if defined ESP32
      ledcAttachPin(ena, AChannel);
      ledcAttachPin(enb, BChannel);
      ledcSetup(AChannel, PWMFreq, RESOLUTION_BITS);
      ledcSetup(BChannel, PWMFreq, RESOLUTION_BITS);
      _ena = AChannel;
      _enb = BChannel;
#endif

#ifndef ESP32
      pinMode(_enb, OUTPUT);
      pinMode(_ena, OUTPUT);
      analogWrite(_ena, LOW);
      analogWrite(_enb, LOW);
#endif
    }


    void move(int leftSpeed, int rightSpeed, int minAbsSpeed) {
      static double aSpeed, bSpeed;

      aSpeed = (double)leftSpeed * _motorAConst;
      bSpeed = (double)rightSpeed * _motorBConst;

      if (abs(aSpeed) < 20.0f) {
        digitalWrite(_in1, LOW);
        digitalWrite(_in2, LOW);
        aSpeed = 0;
      } else if (aSpeed > 0) {
        digitalWrite(_in1, HIGH);
        digitalWrite(_in2, LOW);
      } else {
        digitalWrite(_in1, LOW);
        digitalWrite(_in2, HIGH);
        aSpeed = -aSpeed;
      }

      if (abs(bSpeed) < 20.0f) {
        digitalWrite(_in3, LOW);
        digitalWrite(_in4, LOW);
        bSpeed = 0;
      } else if (bSpeed > 0) {
        digitalWrite(_in3, HIGH);
        digitalWrite(_in4, LOW);
      } else {
        digitalWrite(_in3, LOW);
        digitalWrite(_in4, HIGH);
        bSpeed = -bSpeed;
      }



      if (aSpeed > 255) aSpeed = 255;
      if (bSpeed > 255) bSpeed = 255;

      analogWrite(_ena, (int)aSpeed);
      analogWrite(_enb, (int)bSpeed);

      if (1 == 2) {
        Serial.print("motor output:");
        Serial.print(" left="); Serial.print(leftSpeed); Serial.print('/'); Serial.print(aSpeed);
        Serial.print(" right="); Serial.print(rightSpeed); Serial.print('/'); Serial.print(bSpeed);
        //Serial.print(" abs="); Serial.println(minAbsSpeed);
        Serial.println();
      }
    }


    void move(int speed, int minAbsSpeed)
    {
      int direction = 1;

      if (speed < 0)
      {
        direction = -1;

        speed = min(speed, -1 * minAbsSpeed);
        speed = max(speed, -255);
      }
      else
      {
        speed = max(speed, minAbsSpeed);
        speed = min(speed, 255);
      }

      if (speed == _currentSpeed) return;

      int realSpeed = max(minAbsSpeed, abs(speed));

      digitalWrite(_in1, speed > 0 ? HIGH : LOW);
      digitalWrite(_in2, speed > 0 ? LOW : HIGH);
      digitalWrite(_in3, speed > 0 ? HIGH : LOW);
      digitalWrite(_in4, speed > 0 ? LOW : HIGH);
      analogWrite(_ena, realSpeed * _motorAConst);
      analogWrite(_enb, realSpeed * _motorBConst);

      _currentSpeed = direction * realSpeed;
    }


    void move(int speed)
    {
      if (speed == _currentSpeed) return;

      if (speed > 255) speed = 255;
      else if (speed < -255) speed = -255;

      digitalWrite(_in1, speed > 0 ? HIGH : LOW);
      digitalWrite(_in2, speed > 0 ? LOW : HIGH);
      digitalWrite(_in3, speed > 0 ? HIGH : LOW);
      digitalWrite(_in4, speed > 0 ? LOW : HIGH);
      analogWrite(_ena, abs(speed) * _motorAConst);
      analogWrite(_enb, abs(speed) * _motorBConst);

      _currentSpeed = speed;
    }


    void turnLeft(int speed, bool kick)
    {
      digitalWrite(_in1, HIGH);
      digitalWrite(_in2, LOW);
      digitalWrite(_in3, LOW);
      digitalWrite(_in4, HIGH);

      if (kick)
      {
        analogWrite(_ena, 255);
        analogWrite(_enb, 255);

        delay(100);
      }

      analogWrite(_ena, speed * _motorAConst);
      analogWrite(_enb, speed * _motorBConst);
    }


    void turnRight(int speed, bool kick)
    {
      digitalWrite(_in1, LOW);
      digitalWrite(_in2, HIGH);
      digitalWrite(_in3, HIGH);
      digitalWrite(_in4, LOW);

      if (kick)
      {
        analogWrite(_ena, 255);
        analogWrite(_enb, 255);

        delay(100);
      }

      analogWrite(_ena, speed * _motorAConst);
      analogWrite(_enb, speed * _motorBConst);
    }


    void stopMoving()
    {
      digitalWrite(_in1, LOW);
      digitalWrite(_in2, LOW);
      digitalWrite(_in3, LOW);
      digitalWrite(_in4, LOW);
      digitalWrite(_ena, HIGH);
      digitalWrite(_enb, HIGH);

      _currentSpeed = 0;
    }
};
