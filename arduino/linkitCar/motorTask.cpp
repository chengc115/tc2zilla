#include "Arduino.h"

#include <FreeRTOS.h>
#if defined ARDUINO_ARCH_LINKIT_RTOS
#include <task.h> // RTOS 
#endif
#include "LMotorController.hpp"
#include <PID_v1.h>


#include <ros.h>
#include <geometry_msgs/Twist.h>
geometry_msgs::Twist& getTwist();
// ----------------------------
extern double mpuOutput;
extern boolean rosValueChanged;
extern boolean mpuValueChanged;
// -------------------------------------------
static uint32_t motorTaskDelay = 100;
//static TaskHandle_t motorTaskTid = 0;
// static uint32_t motorTaskParam = 0;
//---------------------------------------
//boolean pidValueChanged;


// -----------------------------------------------
#define CONST_ORIGINAL_SETPOINT 97.28f
#define CONST_PID_P 25.0
#define CONST_PID_I 0.02
#define CONST_PID_D 0.8

double pidOutput = 0;
double originalSetpoint = (CONST_ORIGINAL_SETPOINT);
double pidInput = CONST_ORIGINAL_SETPOINT;
double setpoint = (CONST_ORIGINAL_SETPOINT);
double avgInput = 0;

PID pid(&pidInput, &pidOutput, &setpoint, CONST_PID_P, CONST_PID_I, CONST_PID_D, DIRECT);
// -----------------------------------------------

#ifdef ARDUINO_ARCH_LINKIT_RTOS
uint8_t MOTOR_CONTROL_PINS[] = {
  //  36, 39, 34, 35, 32, 33
  //  0, 1, 2, 3, 4, 5
  // 0, 2, 1, 3, 4,  5,  6
  3, 15, 14, 16, 17, 5
};

#endif
#ifdef ESP32
uint8_t MOTOR_CONTROL_PINS[] = {
  // 26, 25, 23, 32, 35, 34
  //  14, 27, 26,25, 23, 32,
  //  A6, 27, 26,25, 33, A4,
  32, 33, 25, 26, 27, 14
};

#endif


#define MIN_ABS_SPEED 30
#define ROS_POWER_RATE 1500
#define ENA (MOTOR_CONTROL_PINS[0])
#define IN1 (MOTOR_CONTROL_PINS[1])
#define IN2 (MOTOR_CONTROL_PINS[2])
#define IN3 (MOTOR_CONTROL_PINS[3])
#define IN4 (MOTOR_CONTROL_PINS[4])
#define ENB (MOTOR_CONTROL_PINS[5])

class LMotorController motorController(ENA, IN1, IN2, IN3, IN4, ENB, 1, 1);

//-----------------------------------------------
void loopAt1Hz() {
}

void loopAt5Hz() {
}
//-----------------------------------------------
void motorTaskCode(void * pvParam ) {
  static uint32_t wakeup;
  static uint32_t taskDelay;
  static uint8_t state = 0;
  static uint8_t takeLocker = false;
  static uint32_t currentMillis;
  static uint32_t  ts1Hz = 0;
  static uint32_t  ts5Hz = 0;
  static double lRos = 0, rRos = 0;
  static double lOutput = 0, rOutput = 0;

  taskDelay = motorTaskDelay;
  do {
    //if (takeLocker) takeLocker = SPI_LOCKED = false;
    if (pvParam) {
      wakeup = ulTaskNotifyTake( -1L,  taskDelay );
      taskDelay = motorTaskDelay;
    }
    // if (SPI_LOCKED) {
    //   taskDelay = 3;
    //  continue;
    //}
    // takeLocker = SPI_LOCKED = true;

    switch (state) {
      case 0:
        pid.SetMode(AUTOMATIC);
        pid.SetOutputLimits(-255, 255);
        pid.SetControllerDirection(DIRECT);
        state++;
        break;
      case 1:

        // #define USING_MPU
#if defined USING_MPU
        if (mpuValueChanged) {
          pidInput = mpuOutput;
          mpuValueChanged = false;
          pid.Compute();
          // Serial.print("pid="); Serial.println(pidOutput);
        }
        //        motorController.move(pidOutput, pidOutput, MIN_ABS_SPEED);
#endif

#define USING_ROS
#if defined USING_ROS
        if (rosValueChanged) {
          rosValueChanged = false;
          lOutput  = getTwist().linear.x;
          rOutput = getTwist().linear.x;

          lOutput -= getTwist().angular.z / 15.0f;
          rOutput += getTwist().angular.z / 15.0f;

          lOutput *= ROS_POWER_RATE;
          rOutput *= ROS_POWER_RATE;
        }
#endif

#if defined(USING_ROS) && defined(USING_MPU)
        lOutput = lOutput / 2 + pidOutput / 2;
        rOutput = rOutput / 2 + pidOutput / 2;
#endif
        motorController.move((int)lOutput, (int)rOutput, MIN_ABS_SPEED);


        // motorController.move(0, 0, MIN_ABS_SPEED);
        currentMillis = millis();
        if (currentMillis >  ts1Hz) {
          //Serial.printf("motor avg=%8.3f, in=%8.3f, out=%8.3f,set=%8.3f\n", avgInput, pidInput, pidOutput, setpoint);
#if defined ESP32
          Serial.printf("motor lo=%8.3f, ro=%8.3f, pi=%8.3f, po=%8.3f\n",
                        lOutput, rOutput, pidInput, pidOutput);
#else
          Serial.print("motor lo=");Serial.print(lOutput);
          Serial.print(" ro=");Serial.print(rOutput);
          Serial.print(" pid_i=");Serial.print(pidInput);
          Serial.print(" pid_o=");Serial.print(pidOutput);
          
#endif
          loopAt1Hz();
          ts1Hz = currentMillis + 1000;
        }
        if (currentMillis >  ts5Hz) {
          loopAt5Hz();
          ts5Hz = currentMillis + 5000;
        }
    }

  } while (pvParam != NULL);
}
