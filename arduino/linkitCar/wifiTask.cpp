#include "Arduino.h"
#include <FreeRTOS.h>

#ifdef ARDUINO_ARCH_LINKIT_RTOS
#include <LWiFi.h>
#include <task.h> // RTOS   attachInterrupt(INTERRUPT_PIN,RISING);
#include <LRTC.h>
#else
#include <WiFi.h>
#endif

#ifdef ESP32
#include <WiFiMulti.h>
WiFiMulti WiFiMulti;
#endif

#include <WiFiUdp.h>
#include <NTPClient.h>
#include "RTClib.h"
static DateTime rtcNow;
RTC_Millis& getRtc();

#include <ros.h>
#include <geometry_msgs/Twist.h>
// -----------------------------------------
static const TickType_t wifiTaskDelay = 200;
// static TaskHandle_t wifiTaskTid = NULL;
// static uint32_t wifiTaskParam = 0;
// ----------------------------------------
// global variable
static uint8_t wifiStatus = WL_IDLE_STATUS;
// static RTC_Millis rtc;
// static DateTime rtcNow;


////////////////////////////////////////////
struct WIFI_CONFIG_T {
  char *ap;
  char *pass;
  IPAddress *rosServer;
  uint16_t rosPort;
  char *topic;
};

struct WIFI_CONFIG_T WIFI_CONFIG[] = {
  {"7121****", "****1182", new IPAddress(192, 168, 1, 105), 11411, "/cmd_vel"},
  {"chengc-GF62-8RE", "12345678", new IPAddress(10, 42, 0, 1), 11411, "/cmd_vel"},
  {"TT49", "2292****", new IPAddress(192, 168, 65, 65), 11411, "/cmd_vel"},
  {"TT49", "2292****", new IPAddress(192, 168, 65, 65), 11411, "/mobile_base/commands/velocity"},


};
uint8_t WIFI_INDEX = 1;

//--------------------------------------
void cmd_velCallback(const geometry_msgs::Twist& CVel);

// ros::Subscriber<geometry_msgs::Twist> rosSubscriber("/mobile_base/commands/velocity", &cmd_velCallback );
ros::Subscriber<geometry_msgs::Twist> rosSubscriber(WIFI_CONFIG[WIFI_INDEX].topic, &cmd_velCallback );
ros::NodeHandle nh;

bool rosValueChanged = false;
static geometry_msgs::Twist rosTwist;

geometry_msgs::Twist& getTwist() {
  return rosTwist;
}
// -----------------------------------------

void cmd_velCallback(const geometry_msgs::Twist& twist) {
  rosTwist.linear = twist.linear;
  rosTwist.angular = twist.angular;
  rosValueChanged = true;
//  Serial.write('R');
}


void rosLoop() {
  nh.spinOnce();

}
void rosInit() {
  nh.getHardware()->setConnection(*WIFI_CONFIG[WIFI_INDEX].rosServer, WIFI_CONFIG[WIFI_INDEX].rosPort);
  nh.initNode();
  // broadcaster.init(nh);
  // nh.advertise(pub_range);
  // nh.advertise(odom_pub);
  // Another way to get IP
  Serial.print("IP = ");
  Serial.println(nh.getHardware()->getLocalIP());
  nh.subscribe(rosSubscriber);
}


// -------------------------------------------
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 28800, 3600);
// -------------------------------------------

inline bool wifiIsReady() {
  return wifiStatus == WL_CONNECTED;
}
void wifiTaskCode(void * const pvParam) {
  static uint32_t wakeup;
  static uint32_t taskDelay;
  static int8_t state = -1;
  static uint32_t lazyts = 0;
  taskDelay = wifiTaskDelay;
  do {
    if (pvParam) {
      wakeup = ulTaskNotifyTake( -1L,  taskDelay );
      taskDelay = wifiTaskDelay;
    }
    if (!wifiIsReady() && state>0) state = 0;
    switch (state) {
      case -1:
#ifdef ESP32
        // WiFi.disconnect(true);
        WiFi.mode(WIFI_STA);
        WiFiMulti.addAP(WIFI_CONFIG[WIFI_INDEX].ap, WIFI_CONFIG[WIFI_INDEX].pass);
        // wifiStatus = WiFi.begin(WIFI_CONFIG[WIFI_INDEX].ap, WIFI_CONFIG[WIFI_INDEX].pass);
#endif
        state++;
      break;
      case 0:
#ifdef ESP32
        wifiStatus = WiFiMulti.run(); // != WL_CONNECTED
        taskDelay=500;
        // wifiStatus = WiFi.begin(WIFI_CONFIG[WIFI_INDEX].ap, WIFI_CONFIG[WIFI_INDEX].pass);
        // wifiStatus = WiFi.begin(WIFI_CONFIG[WIFI_INDEX].ap, WIFI_CONFIG[WIFI_INDEX].pass);
        // wifiStatus = WiFi.status();
        // if(lazyts<millis()) {
        //   state=-1;
        //   lazyts=millis()+3000;
        // }
#else
        wifiStatus = WiFi.begin(WIFI_CONFIG[WIFI_INDEX].ap, WIFI_CONFIG[WIFI_INDEX].pass);
        // wifiStatus = WiFi.status();
#endif
        Serial.print("W:"); Serial.print(wifiStatus); Serial.print("/");
        Serial.println(WL_CONNECTED);

        if (wifiIsReady()) state++;
        break;
      case 1:
        Serial.print("SSID: "); Serial.println(WiFi.SSID());
        Serial.print("IP Address: "); Serial.println(WiFi.localIP());
        Serial.print("RSSI: "); Serial.println(WiFi.RSSI());
        Serial.println("Wait for NTP, in 15 seconds");
        state++;
        lazyts = millis() + 15000; // timeout 15 SECS
        break;
      case 2:
        getRtc().begin(DateTime(F(__DATE__), F(__TIME__)));
        timeClient.begin();
        state++;
        break;
      case 3:
        if (lazyts < millis()) {
          Serial.println("Sync datetime is timeout!!");
          state++;
        } else if (timeClient.update()) {
          Serial.println("datetime update success!!");
          DateTime rtcNow(timeClient.getEpochTime());
          getRtc().adjust(rtcNow);
#ifdef ARDUINO_ARCH_LINKIT_RTOS
          LRTC.set(rtcNow.year(), rtcNow.month(), rtcNow.day(),
                   rtcNow.hour(), rtcNow.minute(), rtcNow.second());
#endif
          state++;
        }
        taskDelay = 3000;
        break;
      case 4:
        Serial.println("Start ros connection....");
        rosInit();
        state++;
        break;
      case 5:
        taskDelay = 100;
        rosLoop();

        // if (rosValueChanged) {
        //   rosValueChanged = false;
        // }

        if (lazyts < millis()) {
          char buf[32];
          lazyts = millis() + 1000;
#if defined __DEBUG__
          sprintf(buf, " >>> ros linear = % 8.3f, % 8.3f, % 8.3f\n",
                  rosTwist.linear.x, rosTwist.linear.y, rosTwist.linear.z);
          Serial.print(buf);

          sprintf(buf, " >>> ros angular = % 8.3f, % 8.3f, % 8.3f\n",
                  rosTwist.angular.x, rosTwist.angular.y, rosTwist.angular.z);
          Serial.print(buf);
          rtcNow = rtc.now();
          sprintf(buf, "WIFI = % 04d / % 02d / % 02d % 02d : % 02d : % 02d\n",
                  rtcNow.year(), rtcNow.month() , rtcNow.day(),
                  rtcNow.hour(), rtcNow.minute(), rtcNow.second()
                 );
          Serial.print(buf);
#endif
        }
    }
  } while (pvParam != NULL);

}
