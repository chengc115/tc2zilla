
#include "Arduino.h"
#include <FreeRTOS.h>

#if defined ARDUINO_ARCH_LINKIT_RTOS
#include <task.h> // RTOS   attachInterrupt(INTERRUPT_PIN,RISING);
#include <LRTC.h>
#endif


#include <ros.h>
#include <geometry_msgs/Twist.h>
geometry_msgs::Twist& getTwist();

#include "RTClib.h"
static DateTime rtcNow;
// extern RTC_Millis rtc;
RTC_Millis& getRtc();

// #define OLED_RST 16
// define OLED_DC 17
// define OLED_CS  5

// #define OLED_RST 27
// #define OLED_DC 14
// #define OLED_CS  12

#define OLED_RST 0
#define OLED_DC 2
#define OLED_CS  15

#define U8X8_HAVE_HW_SPI
#include <U8g2lib.h>
#include <SPI.h>

// ----------------------------------
extern uint8_t SPI_LOCKED;
extern double pidInput;
extern double pidOutput;
extern double mpuOutput;

// ------------------------------
// static TaskHandle_t oledTaskTid = NULL;
// static uint32_t oledTaskParam = 0;
static TickType_t oledTaskDelay = 1000;
// --------------------------------------

#ifdef ARDUINO_ARCH_LINKIT_RTOS
U8G2_SSD1309_128X64_NONAME2_F_4W_HW_SPI u8g2(U8G2_R0,  /* cs=*/ 10, /* dc=*/ 2, /* reset=*/ 0);
// U8G2_SSD1306_128X64_NONAME_F_4W_HW_SPI u8g2(U8G2_R0,  /* cs=*/ 17, /* dc=*/ 16, /* reset=*/ 15);
#endif

#ifdef ESP32
U8G2_SH1106_128X64_NONAME_F_4W_HW_SPI u8g2(U8G2_R0,
    /* cs=*/ OLED_CS,
    /* dc=*/ OLED_DC,
    /* reset=*/ OLED_RST);
#endif


void oledTaskCode(void * const pvParam) {
  static uint32_t wakeup;
  static uint32_t taskDelay;
  static uint8_t state = 0;
  static uint8_t takeLocker = false;
  static char buf[64];
  static uint32_t loops = 0;

  pinMode(OLED_RST, OUTPUT);
  pinMode(OLED_DC, OUTPUT);
  pinMode(OLED_CS, OUTPUT);

  taskDelay = oledTaskDelay;
  do {
    if (takeLocker) takeLocker = SPI_LOCKED = false;
    if (pvParam) {
      wakeup = ulTaskNotifyTake( -1L,  taskDelay );
      taskDelay = oledTaskDelay;
    }
    if (SPI_LOCKED) {
      taskDelay = 5;
      continue;
    }
    takeLocker = SPI_LOCKED = true;

    switch (state) {
      case 0:
        u8g2.begin();
        u8g2.setFont(u8g2_font_ncenB08_tr);
        //        u8g2.enableUTF8Print();  // enable UTF8 support for the Arduino print() function
        u8g2.firstPage();
        state++;
        break;
      case 1:
        //        u8g2.firstPage();
        u8g2.clearBuffer();
        u8g2.setFont(u8g2_font_ncenB08_tr);

        rtcNow = getRtc().now();
        sprintf(buf, "%04d/%02d/%02d %02d:%02d:%02d %ld",
                rtcNow.year(), rtcNow.month() , rtcNow.day(),
                rtcNow.hour(), rtcNow.minute(), rtcNow.second(),
                loops++);
        u8g2.drawStr(5, 12, buf + 5);

        IPAddress ip = WiFi.localIP();
        sprintf(buf, "%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);
        u8g2.drawStr(5, 24, buf);
        // u8g2.drawStr(0, 32, WiFi.localIP().toStrgin().c_str());

        sprintf(buf, "lx=%6.2f az=%6.2f",
                getTwist().linear.x, getTwist().angular.z);
        u8g2.drawStr(5, 37, buf);

        sprintf(buf, "i=%9.3f o=%9.3f", pidInput, pidOutput);
        u8g2.drawStr(10, 50, buf);

        sprintf(buf, "mpu=%9.3f", mpuOutput);
        u8g2.drawStr(10, 63, buf);

        // Serial.println(buf);
        u8g2.nextPage();
    }
  } while (pvParam != NULL);

}
