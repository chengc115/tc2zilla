#include <Arduino.h>
#include <FreeRTOS.h>
#if defined ARDUINO_ARCH_LINKIT_RTOS
#include <task.h> // RTOS 
#endif
#include <PID_v1.h>

#define MPU6050_DMP_FIFO_RATE_DIVISOR 0x09 // defined in MPU6050_6Axis_MotionApps20.h (200/(0x09+1))Hz
#include "MPU6050_6Axis_MotionApps20.h"

#if defined(ARDUINO_ARCH_LINKIT_RTOS)
#define M_PI (3.1415925)
#define digitalPinToInterrupt(x) (x)
#define _BV(x) (x)
#endif

//-----------------------------------
extern uint8_t I2C_LOCKED;

//-----------------------------------
static const uint32_t mpuTaskDelay = 25;
//static TaskHandle_t mpuTaskTid = 0;
//static uint32_t mpuTaskParam = 0;
boolean dmpReady = false;
double mpuOutput = 0;
boolean mpuValueChanged = false;
// ----------------------------------------------
MPU6050 mpu;
#ifdef ESP32
#define MPU_INTERRUPT_PIN 35
#else
#define MPU_INTERRUPT_PIN 6
#endif
// --------------------------------------------
volatile uint8_t mpuInterrupt = 0;

uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[1050]; // FIFO storage buffer
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU

Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector


////////////////
#if defined(ESP32)
void IRAM_ATTR dmpDataReadyISR() {
  ++mpuInterrupt;
  //  Serial.print("M");
}
#else
void dmpDataReadyISR() {
  ++mpuInterrupt;
}
#endif

void inline setGyroOffsets() {
  // supply your own gyro offsets here, scaled for min sensitivity
  mpu.setXGyroOffset(220);
  mpu.setYGyroOffset(76);
  mpu.setZGyroOffset(-85);
  mpu.setZAccelOffset(1788); // 1688 factory default for my test chip
}

void mpuTaskCode(void * pvParam ) {
  static uint32_t wakeupSignal;
  static uint8_t state = 0;
  static uint32_t taskDelay;
  static uint8_t readCount = 0;
  static uint8_t lockedByMe = false;
  static char buf[80];
  taskDelay = mpuTaskDelay;

  do {
    //        Serial.print("Enter MPU... locked by others? "); Serial.print(lockedByMe);
    //        Serial.print(' '); Serial.print(I2C_LOCKED);
    //        Serial.print(' '); Serial.print(taskDelay);
    //        Serial.println();
    // Serial.print('?');_PULLUP
    if (lockedByMe) lockedByMe = I2C_LOCKED = false;
    if (pvParam != NULL)
      wakeupSignal = ulTaskNotifyTake(-1L, taskDelay);
    if (I2C_LOCKED) {
      // Serial.print('F');
      taskDelay = 5;
      continue;
    }
    // Serial.print('S');
    lockedByMe = I2C_LOCKED = true;

    //    Serial.print("Enter MPU... locked by others? "); Serial.print(lockedByMe);
    //    Serial.print(' '); Serial.print(I2C_LOCKED);
    //    Serial.print(' '); Serial.print(taskDelay);
    //    Serial.println();

#if defined ARDUINO_ARCH_LINKIT_RTOS
    pinMode(MPU_INTERRUPT_PIN, INPUT_PULLUP);
#elif defined ESP32
    //pinMode(MPU_INTERRUPT_PIN, INPUT);
#else
    pinMode(MPU_INTERRUPT_PIN, INPUT_PULLUP);
    // pinMode(MPU_INTERRUPT_PIN, INPUT);
#endif

    taskDelay = mpuTaskDelay;
    switch (state) {
      case 0:
        // Serial.print("Entering init...: sorry no timeout, so you must make sure that the device is connected");
        //if (!mpu.testConnection()) {
        //  Serial.println("try next ...");
        //taskDelay = 5000;
        //  break;
        //}
        // Serial.println("try next ...XX");

        // pinMode(MPU_INTERRUPT_PIN, INPUT_PULLUP);
        // pinMode(MPU_INTERRUPT_PIN, INPUT_PULLDOWN);

        taskDelay = 3000;
        state++;
        break;
      case 1:
        mpu.initialize();
        Serial.println(F("Testing device connections..."));
        Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));
        Serial.println(F("\nSend any character to begin DMP programming and demo: "));
        Serial.println(F("Initializing DMP..."));
        devStatus = mpu.dmpInitialize();

        // supply your own gyro offsets here, scaled for min sensitivity
        setGyroOffsets();

        // make sure it worked (returns 0 if so)
        if (devStatus == 0) {
          // turn on the DMP, now that it's ready
          Serial.println(F("Enabling DMP..."));
          mpu.setDMPEnabled(true);

          // enable Arduino interrupt detection
          Serial.print(F("Enabling interrupt detection (Arduino external interrupt "));
#if defined ESP32
          Serial.print(digitalPinToInterrupt(MPU_INTERRUPT_PIN));
#else
          Serial.print(digitalPinToInterrupt(MPU_INTERRUPT_PIN));
#endif

          // Serial.print((MPU_INTERRUPT_PIN));
          Serial.println(F(")..."));
#if defined ESP32
          attachInterrupt(digitalPinToInterrupt(MPU_INTERRUPT_PIN), dmpDataReadyISR, RISING);
#else
          attachInterrupt(digitalPinToInterrupt(MPU_INTERRUPT_PIN), dmpDataReadyISR, RISING);
#endif
          mpuIntStatus = mpu.getIntStatus();

          // set our DMP Ready flag so the main loop() function knows it's okay to use it
          Serial.println(F("DMP ready! Waiting for first interrupt..."));
          dmpReady = true;

          // get expected DMP packet size for later comparison
          packetSize = mpu.dmpGetFIFOPacketSize();
        } else {
          // ERROR!
          // 1 = initial memory load failed
          // 2 = DMP configuration updates failed
          // (if it's going to break, usually the code will be 1)
          Serial.print(F("DMP Initialization failed (code "));
          Serial.print(devStatus);
          Serial.println(F(")"));
        }
        readCount = 0;
        state++;
      //break;
      case 2:
        if (devStatus == 0) {
          state++;
        } else {
          if (++readCount > 10) state = 0;
          break;
        }
      case 3: // check if no data...
        if (!dmpReady) {
          state = 0;
          break;
        }
        // Serial.print("Check MPU"); Serial.println(mpuInterrupt);
        // not use interrupt
        
        fifoCount = mpu.getFIFOCount();
        if (mpuInterrupt == 0 && fifoCount<packetSize) break;
        mpuInterrupt = 0;
        // if ((fifoCount = mpu.getFIFOCount()) < packetSize) break;
        mpuIntStatus = mpu.getIntStatus();

        if (1 == 2) {
          Serial.print("MPU status="); Serial.print(mpuIntStatus);
          Serial.print(" len="); Serial.print(fifoCount);
          Serial.println();
        }
        if ((mpuIntStatus & _BV(MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024) {
          // overflow...
          mpu.resetFIFO();
          // fifoCount = mpu.getFIFOCount();
          sprintf(buf, "FIFO overflow: %04x, %04x, %d\n", mpuIntStatus, MPU6050_INTERRUPT_FIFO_OFLOW_BIT, fifoCount);
          Serial.print(buf);
          // Serial.println(F("FIFO overflow!"));
          // state = 0;
          break;
        }
        state++;
      case 4:
        // while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();
        readCount = 0;
        while (fifoCount >= packetSize) {
          delay(1);
          mpu.getFIFOBytes(fifoBuffer, packetSize);
          fifoCount -= packetSize;
          readCount++;
          // fifoCount = mpu.getFIFOCount();
        } // we only want the latest value

        if (fifoCount != 0) {
          mpu.resetFIFO();
          break;
        }
        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &q);
        mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);

        mpuOutput = (ypr[1] * (180.0f / M_PI) + 270.0f);
        if (mpuOutput > 360.0f)mpuOutput -= 360.0f;
        mpuValueChanged = true;

        static uint8_t lazy = 0;
        if ( (++lazy) > 10) {
          lazy = 0;
          if (1 == 2) {
            sprintf(buf, "ypr:\t%02d\t%8.3f\t%8.3f\t%8.3f\t%8.3f\n",
                    readCount, mpuOutput,
                    ypr[0] * (180.0 / M_PI),
                    ypr[1] * (180.0 / M_PI),
                    ypr[2] * (180.0 / M_PI));
            Serial.print(buf);
          }
          if (1 == 1) {
            sprintf(buf, "quat: %2d, %8.3f\t%8.3f\t%8.3f\t%8.3f\t%8.3f\n",
                    readCount, q.w, q.x, q.y, q.z, mpuOutput);
            Serial.print(buf);
          }
        }
    }
    // Serial.println("Leave init");
    if (state >= 3) state = 3; // continue check data

  } while (pvParam != NULL);
}
