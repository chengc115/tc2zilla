
#ifdef ARDUINO_ARCH_LINKIT_RTOS
#include <LWiFi.h>
#else
#include <WiFi.h>
#endif

#include <WiFiUdp.h>
#include <NTPClient.h>

#include "Arduino.h"

#include <FreeRTOS.h>
#include <task.h> // RTOS LIB

// ros lib -------------------------
#include <ros.h>
// messages
#include <geometry_msgs/Twist.h>
// ----------------------------------------
// global variable
static uint8_t wifiStatus = WL_IDLE_STATUS;
// -----------------------------------------
static TaskHandle_t wifiTaskTid = NULL;
static uint32_t wifiTaskParam = 0;
static TickType_t wifiTaskDelay = 200;

////////////////////////////////////////////
struct WIFI_CONFIG_T {
  char *ap;
  char *pass;
  IPAddress *rosServer;
  uint16_t rosPort;
};

struct WIFI_CONFIG_T WIFI_CONFIG[] = {
  {"71216457", "22611182", new IPAddress(192, 168, 1, 105), 11411},
};
uint8_t WIFI_INDEX = 0;

//--------------------------------------
ros::NodeHandle nh;
void cmd_velCallback(const geometry_msgs::Twist& CVel);
ros::Subscriber<geometry_msgs::Twist> rosSubscriber("/mobile_base/commands/velocity", &cmd_velCallback );
bool rosDataReceived = false;
geometry_msgs::Twist rosTwist;


// -----------------------------------------

void cmd_velCallback(const geometry_msgs::Twist& twist) {
  rosTwist.linear = twist.linear;
  rosTwist.angular = twist.angular;
  rosDataReceived = true;
}


void rosLoop() {
  nh.spinOnce();

}
void rosInit() {
  nh.getHardware()->setConnection(*WIFI_CONFIG[WIFI_INDEX].rosServer, WIFI_CONFIG[WIFI_INDEX].rosPort);
  nh.initNode();
  // broadcaster.init(nh);
  // nh.advertise(pub_range);
  // nh.advertise(odom_pub);
  nh.subscribe(rosSubscriber);
}


// -------------------------------------------
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 28800, 3600);
// -------------------------------------------

inline bool wifiIsReady() {
  return wifiStatus == WL_CONNECTED;
}
void wifiTaskCode(void * const pvParam) {
  static uint32_t wakeup;
  static uint32_t taskDelay;
  static uint8_t state = 0;

  taskDelay = wifiTaskDelay;
  do {
    if (pvParam) {
      wakeup = ulTaskNotifyTake( -1L,  taskDelay );
      taskDelay = wifiTaskDelay;
    }
    if (!wifiIsReady()) state = 0;
    switch (state) {
      case 0:
        wifiStatus = WiFi.begin(WIFI_CONFIG[WIFI_INDEX].ap, WIFI_CONFIG[WIFI_INDEX].pass);
        wifiStatus = WiFi.status();
        state++;
        if (!wifiIsReady()) state++;
        break;
      case 1:
        Serial.print("SSID: "); Serial.println(WiFi.SSID());
        Serial.print("IP Address: "); Serial.println(WiFi.localIP());
        Serial.print("RSSI: "); Serial.println(WiFi.RSSI());
        state++;
        break;
      case 2:
        timeClient.begin();
        state++;
        break;
      case 3:
        rosInit();
        state++;
        break;
      case 4:
        // Serial.println(timeClient.getFormattedTime());
        rosLoop();
        taskDelay = 10;
        if (rosDataReceived) {
          rosDataReceived = false;
        }
        char buf[32];

        sprintf(buf, "ros linear= %8.3f, %8.3f, %8.3f\n",
                rosTwist.linear.x, rosTwist.linear.y, rosTwist.linear.z);
        Serial.print(buf);

        sprintf(buf, "ros angular= %8.3f, %8.3f, %8.3f\n",
                rosTwist.angular.x, rosTwist.angular.y, rosTwist.angular.z);
        Serial.print(buf);

    }
  } while (pvParam != NULL);

}
