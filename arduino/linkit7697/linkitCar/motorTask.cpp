#include "Arduino.h"

#include <FreeRTOS.h>
#include <task.h> // RTOS 
#include "PID_v1.h"
//#include "LMotorController.h"


// -------------------------------------------
const uint32_t motorTaskDelay = 10;
TaskHandle_t motorTaskTid = 0;
uint32_t motorTaskParam = 0;
// ------------------------------------------------
#define CONST_PID_P 25.0
#define CONST_PID_I 0.02
#define CONST_PID_D 0.8

#define CONST_ORIGINAL_SETPOINT 97.28f



double pidOutput = 0;
double originalSetpoint = (CONST_ORIGINAL_SETPOINT);
double pidInput = CONST_ORIGINAL_SETPOINT;
double setpoint = (CONST_ORIGINAL_SETPOINT);
double avgInput = 0;
PID pid(&pidInput, &pidOutput, &setpoint, CONST_PID_P, CONST_PID_I, CONST_PID_D, AUTOMATIC);

// -------------------------------------------------- -
uint8_t MOTOR_CONTROL_PINS[] = {
  36, 39, 34, 35, 32, 33
};

#define ENA MOTOR_CONTROL_PINS[0]
#define IN1 MOTOR_CONTROL_PINS[1]
#define IN2 MOTOR_CONTROL_PINS[2]
#define IN1 MOTOR_CONTROL_PINS[3]
#define IN4 MOTOR_CONTROL_PINS[4]
#define ENB MOTOR_CONTROL_PINS[5]

// LMotorController motorController(ENA, IN1, IN2, ENB, IN3, IN4, 0.6, 0.6);



//-----------------------------------------------
void loopAt1Hz() {
}

void loopAt5Hz() {
}
//-----------------------------------------------
void motorTaskCode(void * pvParam ) {
  static uint32_t wakeup;
  static uint32_t taskDelay;
  static uint8_t state = 0;
  static uint8_t takeLocker = false;
  static uint32_t currentMillis;
  static uint32_t  ts1Hz = 0;
  static uint32_t  ts5Hz = 0;



  taskDelay = motorTaskDelay;
  do {
    //if (takeLocker) takeLocker = SPI_LOCKED = false;
    if (pvParam) {
      wakeup = ulTaskNotifyTake( -1L,  taskDelay );
      taskDelay = motorTaskDelay;
    }
    // if (SPI_LOCKED) {
    //   taskDelay = 3;
    //  continue;
    //}
    // takeLocker = SPI_LOCKED = true;

    switch (state) {
      case 0:
        state++;
        break;
      case 1:
        pid.Compute();
        // motorController.move(pidOutput, pidOutput, MIN_ABS_SPEED);

        currentMillis = millis();
        if (currentMillis >  ts1Hz) {
          // Serial.printf("pid avg=%8.3f, in=%8.3f, out=%8.3f,set=%8.3f\n", avgInput, pidInput, pidOutput, setpoint);
          loopAt1Hz();
          ts1Hz = currentMillis + 1000;
        }
        if (currentMillis >  ts5Hz) {
          loopAt5Hz();
          ts5Hz = currentMillis + 5000;
        }
    }

  } while (pvParam != NULL);
}
