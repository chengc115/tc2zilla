
#include "Arduino.h"

#include <FreeRTOS.h>
#include <task.h> // RTOS   attachInterrupt(INTERRUPT_PIN,RISING);

#include <Wire.h>
#include <ros.h>
#include <geometry_msgs/Twist.h>
#include <LiquidCrystal_I2C.h>
#include <LWiFi.h>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <LWatchDog.h>

// -----------------------------------
static uint8_t I2C_LOCKED = false;
static uint8_t SPI_LOCKED = false;


/////////////////////////////////////////////////////////////

static const uint32_t wifiTaskDelay=100;
static TaskHandle_t wifiTaskTid;
static uint32_t wifiTaskParam;
extern void wifiTaskCode(void * pvParam );

extern const uint32_t lcdTaskDelay;
extern TaskHandle_t lcdTaskTid;
extern uint32_t lcdTaskParam;
extern void lcdTaskCode(void * pvParam );

extern const uint32_t oledTaskDelay;
extern TaskHandle_t oledTaskTid;
extern uint32_t oledTaskParam;
extern void oledTaskCode(void * pvParam );


extern const uint32_t motorTaskDelay;
extern TaskHandle_t motorTaskTid;
extern uint32_t motorTaskParam;
extern void motorTaskCode(void * pvParam );

////////////////////////////////////////////////////////////////////////////////////
#include <LRTC.h>
#include "RTClib.h"
RTC_Millis rtc;

void initRTC() {
  LRTC.begin();
  LRTC.set(2019, 8, 12, 23, 22, 30);
  rtc.begin(DateTime(F(__DATE__), F(__TIME__)));
  DateTime now = rtc.now();
  LRTC.set(now.year(), now.month(), now.day(),
           now.hour(), now.minute(), now.second());
}

// ---------------------------------
void initWatchDog() {
  LWatchDog.begin(10);
}


/////////////////////////////////////////////////////////////
#define INTERRUPT_PIN 6

void pin_change(void) {

}

void initISR() {
  pinMode(INTERRUPT_PIN,INPUT_PULLUP);
  attachInterrupt(INTERRUPT_PIN, pin_change, RISING);
}
/////////////////////////////////////////////////////////////
void setup() {
  Serial.begin(115200);

  initISR();
  initRTC();
  initWatchDog();
  xTaskCreate(oledTaskCode, (const char *)"oled", 1024, (void *)&oledTaskParam, tskIDLE_PRIORITY, &oledTaskTid);
  xTaskCreate(wifiTaskCode, (const char *)"wifi", 4096, (void *)&wifiTaskParam, tskIDLE_PRIORITY, &wifiTaskTid);

}



//////////////////////////////////////////


// xTaskCreate( lcdTask, "lcd",128,lcdParam,lcd

void loop() {
  // put your main code here, to run repeatedly:

  //  oledTaskCode(NULL);
  delay(1000);
  LWatchDog.feed();

}
