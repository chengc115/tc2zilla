# 自造者的機器人


#  專案說明

## 目的

本專案以自造者的角度, 自行建立一組ROS機器人執行環境, 做為瞭解ROS系統構時所需的入門知識與基本環境需求與說明。

> 專案以ROS做為通訊核心基楚， 透過整合 Linux上的 ROS 核心系統， 與 EPS32／Linkit7659 等具有 WIFI能力與較大處理能力的Arduino做為機器人的玩具車所建構出來的 ROS 整合環境.


> 本專案透過ROS環境, 進行建構在ARDIINO開發版上的玩具車, 以做為瞭解ROS的入門

> 控制器説明: 控器的模擬使用 Gazebo或rviz 做為模擬器, 透過 Gazebo提供的TOOL, 如turtlebot3_teleop 透過網路控制機器人(玩具車)

## 本件説明內容
1. 安裝 ROS 主系統環境
2. 建構 ARDUINO 受控端 機器人環境
3. 其他必要知識

# ROS 安裝

專案基於ROS第一版做為研究, 其相關介紹請參考原始網站說明,  [ https://wiki.ros.org/ ]  文件中若無必要, 不對ROS做說明


ORS安裝包含

| 系統 | 說明 |
|-----|----|
|主機系統| 您使用的機器,
| 虛擬系統| DOCKER, 或 VIRTUALBOX, 或VMWARE, 將做為執行ROS的GUEST主機的環境|
| ROS系統所需的主機| 1. 如主機系統ikfnLINUX, 可以直接參考建構內容, 直接安裝軟體<br>2.透過虛擬機(如DOCKER, VIRTUALBOX, VMWARE) 建立一套LINUX上<br>3.以UBNTU 16.04, 18.04較不易出錯 |
| ROS系統 | 專案以 ROS 第一版為主, 目前有套可以使用<br> UBNTU 14.06 + kinetic <br> UBNTU 18.04 + melodic <br> ROS2: 本專案不適用|
| ROS 軟體 | desktop-full(己包含大部份軟體), gazebo, rosserial |
| Arduino (ROBOT) | 採ESP32品片, 或LINKIT697, 或AMIBA |
|ARDUINO LIB | rosserial, pid, ...|




## 主機系統

+ 建議使用LINUX系統做為作系統系
+ RAM: 8BG以上, 4核(THREAD) CPU,
+ HD: 有60 ~ 120GB給手虛擬機使用 (使用SSD, 主機效能可多出很多)
+ 因為ROS採用虛擬環境執行, 所以也無特別限制
+ 如想在WINDOWS下建立雙系系, 如無特別應用需求, LINUX系統可使用 60GB即可, 
+ 建議使用UBNTU 18.04, DESKTOP版
+ UBNTU : https://ubuntu.com/download
+ DEBIAN: https://www.debian.org/
+ 如想要加裝 OPENCV, PYTHON3.7 ...等, 需擴大到 100GB以上


## GUEST系統安裝

以上分為以模擬器安裝與 DOCKER安裝

### 模擬器 (VIRTUAL BOX, VMWARE)
步驟: 以virtualbox做為說明
1. 下載模擬器 : 參考主機系統, 下載相對應的安裝檔 (virtualbox)
    + WINDOWS: [ https://www.virtualbox.org/ ] 下載安裝
    + UBUNTU: 
        1. 透過 apt install 安裝;
        2. 安裝最新一版 (GOOGLE: ubuntu virtualbox install)
    + MAC OS: (待補)
2. 請勿在公司或不是私人機器上裝未授權的模擬器
    > oracle 最近有整頓的現像....也請避免透過公司網路安裝virtualbox
3. 安裝VIRTUALBOX

### OS下載
+ 確認選用ROS版本, melodic 或 kinetic, 64BIT作業系統

| kinetic | melodic |
|:-----:|:-----:|
| UBUNTU 16.04 | UBUNTU 18.04 |

透過  https://ubuntu.com/download 下載存成ISO檔

### 安裝基礎OS
0. 主機器需能上網
1. 建立一台虛擬機, 建立後不急著開機, 先檢核以下內容
    >LINUX64位元
    >CPU: 能給多少就多少, RAM: 4GB以上, HD: 120G,<br>
    >顯卡記錄體: 64M以上<br>
    >光碟機: 是否把下載的IOS檔掛進來<br>
    >硬碟: 啟用主機CACHE機制<br>

2. 開機進行安裝, 裝過程請記住安裝時的 **帳密** 
4. 裝完後重開機

### 安裝擴充元件

+ virtualbox: 在安裝完成後, 請記得安裝 virtualbox PLUGINS
+ 讓使用者可以方便使用: 
    > sudo usermod -a -G vboxusers $USER


### 安裝所需軟體 (擇一執行)

#### melodic, UBNTU 18.40
``` 
sudo apt update
sudo apt upgrade -y
sudo apt install -y tmux git wet curl lsb-reslease lsb-core zip vim apt-utils libignition-math2

sudo echo "deb http://packages.ros.org/ros/ubuntu `lsb_release -cs` main" > /etc/apt/sources.list.d/ros-latest.list'

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116 && \

sudo apt-get update 

sudo apt-get install -y ros-melodic-desktop ros-melodic-desktop-full python-rosinstall

sudo rosdep init


sudo echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list'
sudo wget http://packages.osrfoundation.org/gazebo.key -O - | sudo apt-key add - 

sudo apt-get update 
sudo apt-get install -y gazebo9 libgazebo9-dev
sudo apt-get install -y ros-melodic-gazebo-ros-pkgs ros-melodic-gazebo-ros-control


sudo apt-get install -y zip vim apt-utils python-rosinstall python-rosinstall-generator python-wstool build-essential  python-rosdep python-wstool
sudo apt-get install -y ros-melodic-rosserial libignition-math2

sudo apt-get install -y ros-melodic-rosserial ros-melodic-turtlebot3 ros-melodic-turtlebot3-example ros-melodic-turtlebot3-fake ros-melodic-turtlebot3-simulations ros-melodic-turtlebot3-teleop

sudo apt-get upgrade -y

```
最後把執行環境寫入登入自動執行檔
````
echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
````

如需要開機說明, 登入時自動顯示一些常用的提示: 
````

echo << EOF >> ~/.bashrc
echo "cat << USAGE"  >> ~/.bashrc
echo "USAGE: 1.) TURTLEBOT model, 2.)change to source folder, 3/) setup env, 4.) run it!!"  >> ~/.bashrc
echo "\\texport TURTLEBOT3_MODEL=burger|waffle|wafflepi"  >> ~/.bashrc
echo "\\tcd /opt/ros_ws"  >> ~/.bashrc
echo "\\tsource ./devel/setup.bash"  >> ~/.bashrc
echo "\\troslaunch turtlebot3_gazebo turtlebot3_world.launch" >> ~/.bashrc
echo "\\troslaunch turtlebot3_teleop turtlebot3_teleop_key.launch"  >> ~/.bashrc
echo "\\trosrun rviz rviz -d \\\`rospack find turtlebot3_navigation\\\`/rviz/turtlebot3_nav.rviz"  >> ~/.bashrc
echo "USAGE"  >> ~/.bashrc

````

#### 檢查是否安裝成功
1. 虛擬機重新開機, 登入
2. 開啟 "終端機" (terminal)
2. $source /opt/ors/melodic/setup.bash (如果沒做環境設定)
    1. 第一個TAB執行: $roscore
    2. 再開另一個TAB執行: (待補)
    2. 第三個TAB執行: (待補)


#### kinetic, UBNTU 16.40

安裝時如有錯誤, 可勿略
```
sudo apt update
sudo apt upgrade -y #請勿昇版!!!

# OS軟體
sudo apt install -y apt-utils  dialog whiptail zip vim apt-utils python-rosinstall python-rosinstall-generator python-wstool build-essential  python-rosdep python-wstool  debian-keyring linux-source-4.15.0 

# ROS
sudo apt install ros-kinetic-desktop-full 

# 其它軟體
sudo apt-get upgrade -y
sudo apt-get install -y apt-utils
sudo apt-get install -y dialog whiptail # librealsense2-dkms
sudo apt-get install -y zip vim apt-utils python-rosinstall python-rosinstall-generator python-wstool build-essential  python-rosdep python-wstool
sudo apt-get install debian-keyring linux-source-4.15.0
sudo apt-get install -y ros-kinetic-rosserial 
sudo apt-get install -y ros-kinetic-kobuki-ftdi 
sudo apt-get install -y ros-kinetic-ar-track-alvar-msgs 
sudo apt-get install -y ros-kinetic-turtlebot ros-kinetic-turtlebot-apps ros-kinetic-turtlebot-interactions ros-kinetic-turtlebot-simulator 

sudo bash -c (. /opt/ros/kinetic/setup.sh && rosdep init && rosdep update )

echo "source /opt/ros/kinetic/setup.bash" >> ~/.ba:shrc

```

#### 檢查是否安裝成功
1. 虛擬機重新開機, 登入
2. 開啟 "終端機" (terminal)
2. $source /opt/ors/kinetic/setup.bash (如果沒做環境設定)
    1. 第一個TAB執行: $roscore
    2. 再開另一個TAB執行: (待補)
    2. 第三個TAB執行: (待補)


## 採用DOCKER安裝(不使用GUEST系統)


. LINUX : DEBIAN. UBUNTU都可, 不建議太新版本, 會有安裝相容問題不易克服, UBUNTU 18.04是比較好的選擇
. 因ROS將在DOCKER內執行, 因些在HOST主機上不用安裝ROS
. ROS將安裝在DOCKER IMAGE之中
. ROS將從網路上直接下載, 無需安裝 (但也可以從OS層開始安裝起)


## 作業系統安裝

為加快首次接觸者可以進入狀況, 本系統會採用 DOCKER, 做為虛擬機器核心, 
1. IMAGE檔案己經完整安裝測試, 後續不用再檢核安裝是否有誤
2. 所需的PACKAGE在DOCKERFILE中己完整載入
3. 重建時快速
4. 必需有DOKCER基本能力


## DOKCER 安裝(建議使用)
DOCKER為輕量級系統, 在開新系統時很快速, 建議使用, 但只有在LINUX下才能發揮其功力. LINUX的主機系統版只要能安裝DOCKER軟體, 其版本都不拘 

### 主機基環境需求:
+ UBUNTU或DEBIAN都可
+ GOOGLE: ubuntu docker 安裝
+ 最新版愛好者, 可以在 [ https://docs.docker.com/install/linux/docker-ce/ubuntu/ ] 找到安裝說明
+ 由OS PACKAGE安裝, sudo apt install docker.io 
+ 或安裝最新版(參考網路說明)
+ 安裝後把自己加到GORUP中: sudo usermod -aG docker gtwang
+ 測試: docker run hello-world
+ 刪除 : 使用 docker ps, docker stop, docker rm, docker rmi

    DOCKER存儲空間:
    > image: 可以視為安裝完成的作業系統, 內包含這台機器必要的軟體及設定
    > container: 視為在IMAGE做為基礎後, 透過DOCKER開機後, 後續機器要存放的內容 包含被修改的檔案...但被修修改的檔案一, 只寫到 container中, IMAGE不會被修改
    > 建立IMAGE時, 可以BASE ON其它的IMAGE開始建立(節省建構時間)
    >目錄或網路可以透過執行時設定對應出來, 到主機器上

#### DOKCER 小CMMAND

    當透過docker 去執行一個docker image (含tabName) , docker會先在本機中查看該IMAGE是否己存在, 如果沒有, 會再從DOCKERHUB網站下載，並存入 LOCAL HD , 並供日後使用， 直到被刪除為止（docker rmi image_name), 當要實際執行一台機器時， docker 會自動建一個名為 （由 --name 參數指定）或自動建立UNIQUE名稱的執行容器(container))。 建立完成後，日後就在這個container 的RUNNING SPACE下執行該系統.

+ 下載  : docker pull imag_name:tag 
+ 啟動 :  docker **run** --name container_name  image_name:tag
+  現有多少機器容器被執行(查看continer_name)): docker ps 
+  有多少機器容器被建立(查看continer_name)): docker ps  -a
+ 刪除機器容器(container) : docker rm continer_name
+ 己下載多少機器IMAGE/image_name: docker images
+ 刪除己下載的IMAGE: docker rmi image_name
+ 停止執行中的container: docker stop container_name
+ 啟動被停止的container: docker **start** container_name
+ 在執行中的container中進入console: doker **exec** -it container_name **/bin/bash**

#### 進階參數:(參閱執行的SMAPLE SHELL)
+  在建立container時, 可以指定的參數
+ -v ; --volumn : 指定HIST與CONTAINER間檔案的MAPPING, 通常使用在把CONTAINER中開發的程式MAPPING出來, 以免未來誤刪時隨著container一起被刪
+  -p; --port : 把TCPIP PORT做MAP, 到主機的PORT, 以利從外界跟container相連
+ -e; --env : 設定container的環境變數, 通常要到 docker-hub 上看才可以找到有那些參數可以設


### 建構前說明

+ 自行建立: BASE ON OS IMAGE, DOCKER BUILD, ... 寫SCRIPT 
+ 從網路上下載己建立完成的IMAGE, 
+ 不用時再去刪除, 但需自行確認container_name, image_name
````
# get an image, for exampel melodic-desktop-full 
docker pull melodic-desktop-full 
# remove an image, free some space
docker ps -a
docker stop {container_name} # if running
docker rm {container_name}
docker images
docker rmi {image_name:tag}
````

## *與X11 連結*
+ 因DOCKER並無GUI界面, 所以必需透過特殊方式執行, 參考以下說明
+ 當主機閞機後, X11連結會失效, CONTAINER需重建
+ 如果有NVIDIA顯卡, 需再參考網路設定,(會使你的顯示加快)

### 建構ROS

1. 透過己存在的IMAGE執行(本專案因需增加軟體, 並不適用)
```
xhost +  # 允許從別人可以在你的SCREEN上顯示
docker run -it \
    -p 11411:11411 \
    -p 11311:11311 \
    --name=rosmx \
    --device=/dev/dri:/dev/dri  \
    --env="DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --env="XAUTHORITY=${XAUTH}" \
    --volume=$XSOCK:$XSOCK:rw \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --volume=/home/$USER/ros:/root/ros_ws \ #目錄對應出來
    osrf/ros:melodic-desktop-full  roscore
    
```
    執行說明
    1. -p: 把主機的TCPIP PORT 指定給 CONTAINER
    2. --name: 指定 CONTAINER 名稱
    3. --device:　
    4. --env: 指定環境變數
    5. --volume: 把主機的檔案, 指定到GUTST的特定目錄
    6. SAMPEL中把 ROS中的 /root/ros_ws MAP到使用者目錄, 目的是把GUEST中的檔案轉存到外部, 
    A) 檔案備份 B)如果CONTANER需重建時, 檔案仍然存在
    7 osrf/ros:melodic-desktop-full


2. 自行建立所需要的IMAGE
    + 參閱 /docker/docker-melodic 或 /docker/docker-kinetic 
    + Dockerfile : IMAGE建構檔
    + kx.sh, mx.sh : 在IMAGE建立完成後, 啟動 ROS

    如何建構IMAGE
>開啟TERMINAL

    ```
    cd ~ros/docker/docker-melodic #目錄
    docker build . -trosmx # -t image 名稱, 為rosmx 或roskx
    ```
    m或k: ROS版本
    x: for X11 使用

3. DOCKER 測試
使用SHELL SCRIPT執行 (需設為可執行 $chmod 0755 mx.sh)
````
    cd ~ros/docker/docker-melodic #目錄
    ./mx.sh #開啟VM
````
>開啟第二個TERMINAL

    docker exec -it rosmx bash
    roslanunc ....

4. 如何刪除
````
docker ps -a
docker rm rosmx #請確認是否要刪除, 一但刪除, 資料會LOST
docker images
docker rmi rosmx #請確認是否要刪除, 建構的OS會不見了!!
````
> 如果有透過volume對應出來, 資料是可以被保留的
> 可以使用 docker cp 把資料備份出來


#### 測試 kinetic
    docker exec -it container_name /binbash
    suorce /opt/ros/kinetic/setup.bash
    rviz

#### 測試 melodic
    docker exec -it container_name /binbash
    suorce /opt/ros/melodic/setup.bash
    rviz

#### 結束
        docker stop container_name

#### 重新執行
        docker start container_name

#### 移除(container)
        docker rm container_name

#### 移除IMAGE
        docker rm container_name

## 主機重開機
 > 在重開機後, 因X11的連結會失效,  conainer 必需重新建立(xhost .....之後的命令))
 > docker start container_name
 > 或直接執行 nx.sh, mx.sh

### 使用NVIDIA顯卡
    需以 nvidia-docker 取代docker即可

## DOCKER引用的PACKAGE
參考Dockerfile 內容, 簡單說明如下

```
#UBNTU18.04 就不用裝了melodic-desktop-full
FROM osrf/ros:melodic-desktop-full
...
#安裝OS的工具, 或開發工具等等....
RUN apt-get update && \
    apt-get install -y vim \
    tmux \
    git \
    wget curl \
    lsb-release \
    lsb-core
...
# Install ROS
# 因己使用FULL了, 所以不再裝任何軟體

# Install Gazebo # 安裝模擬軟體
RUN sh -c 'echo "deb http:.....
....

#其它軟體
RUN apt-get install -y zip vim 


# 使用環境設定
RUN echo "export TURTLEBOT3_MODEL=burger" >> ~/.bashrc
...
RUN echo << EOF >> ~/.bashrc
......
```
透過 docker build . -t{IMAGE_NAME} 進行建立

所需空間約 3.5G


# ARDUINO ROBOT

## 軟體環境

1. 使用LINKIT7697, EPS32, AMEBA, 等有WIFI的MCU
2. ARDUINO IDE 開發軟體
3. 使用 rosserial_arduino_lib
4. LIB會被下載𩰊 ~/Arduino/libraries/rosserial_....
5. 修改 ros.h, 出現ESP8266之下增加一組 ESP32 
6. 把 ESP8266Hardware.CPP COPY為Linkit7697hardware.CPP
7. 把 ESP8266Hardware.CPP COPY為Linkit7697hardware.CPP
8. COMPILE SAMPLE

## 玩具車
1. MCU
2. 雙輪車一組
3. 碼達趨動CHIP (LN293..待確認)

## 程式碼
~/linkit7697/linkCar.ino
```
配線:參考源碼內的設定

ENA ==> PORT 0 (PWM)
IN1 ==> PORT 1 (LEFT MOTOR +)     
IN2 ==> PORT 2 (LEFT MOTOR -) 
IN3 ==> PORT 3 (RIGHT MOTOR -)
IN4 ==> PORT 4 (RIGHT MOTOR +)
ENB ==> PORT 5 (PWM)
```
## TESTING
1. 啟動 ROS: cd ~/ros/docker/docker-meloic; ./kx.sh
2. 啟動模擬器:
    >docker exec -it roskx bash<br>
    >roslaunch turtlebot3_gazebo turtlebot3_world.launch<br>
    >roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch
    >rosrun rosserial_python serial_node.py tcp

3. 確認網路, 確認IP是否正確; 
4. 確認ARDUINO輸出
5. 操作turtlebot3_teleop, 讓玩具車行走


# 未來發展雙輪自平車與ROS結合


# 研究結論
1. ROS安裝 : 
2. ROBOT
3. 控制端程式整合
4. 機電整合: 電源, 碼力, ROBOT回饋
5. 控制端與受控端整合
6. ARDUINO 透過 rosserial 做為GATEWAY整合


# 未來發展
1. 增加偵測能力
2. 自動辨識
3. 自動駕駛
4. 自動走完操場
5. 評估採用 RASBERRYPI做為控制中心
6. ARDUINO以ROS2整合, 滅少一個FAULT POINT (rosserial)