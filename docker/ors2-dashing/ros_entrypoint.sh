#!/bin/bash
set -e

# setup gazebo environment
[ -f /usr/share/gazebo/setup.sh ] && source "/usr/share/gazebo/setup.sh"
exec "$@"

# setup ros2 environment
source "/opt/ros/$ROS_DISTRO/setup.bash"
exec "$@"
