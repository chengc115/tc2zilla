# How to use

+ download all files
+ make image
+ start container
+ rutn test..

# Donwload and check

+ check file

> $ ls 

    Dockerfile
    r2.sh
    sources.list

+ mofidy sources site

>\# for reducing building time, replace sources.list with your host's sources site!!<br>
$ cp /etc/apt/sources.list .


# Build image

+ build the image named: ros2x

> $ docker build . -t {your_tag_name}
  
    chengc@TV2730-OptiPlex-3050:~/ros/ros2$ docker build . -t ros2x
    
    .....
    
    Removing intermediate container 91f53d461235
     ---> 272f4625bf46
    Step 20/32 : WORKDIR /
     ---> Running in e9fd6b0a4d36
    Removing intermediate container e9fd6b0a4d36
     ---> 8cccd768605c
    Step 21/32 : EXPOSE 11411
     ---> Running in 49243382070e
    Removing intermediate container 49243382070e
     ---> d901b5ad6b5e
    Step 22/32 : EXPOSE 11311
    
    ... ....
    
    Removing intermediate container 263917a731ed
     ---> 7f91d172650b
    Successfully built 7f91d172650b
    Successfully tagged ros2:latest



# Start container

+ create a container and run

> \# in a terminal tab<br>
$ ./r2.sh
    
    chengc@TV2730-OptiPlex-3050:~/ros/ros2$ docker exec -it ros2x /bin/bash
    Usage:
    	TURTLEBOT model, burger|waffle|wafflepi
    	export TURTLEBOT3_MODEL=burger
    Example command:
    	export TURTLEBOT3_MODEL=burger|waffle|wafflepi
    	gazebo --verbose /opt/ros/dashing/share/gazebo_plugins/worlds/gazebo_ros_diff_drive_demo.world
    	gazebo --verbose /opt/ros/dashing/share/gazebo_plugins/worlds/gazebo_ros_tricycle_drive_demo.world
    	ros2 topic pub /demo/cmd_demo geometry_msgs/Twist '{linear: {x: 1.0}}' -1
    	sleep 2
    	ros2 topic pub /demo/cmd_demo geometry_msgs/Twist '{linear: {x: 0.0}}' -1
    root@97836d32d882:/# 

+ check container name

> \# open a new terminal tab<br>
$ docker ps

    chengc@TV2730-OptiPlex-3050:~/ros/ros2$ docker ps
    CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS              PORTS                                                                                            NAMES
    97836d32d882        ros2                   "/ros_entrypoint.sh …"   11 minutes ago      Up 11 minutes       0.0.0.0:11311->11311/tcp, 0.0.0.0:11411->11411/tcp, 11345/tcp                                    *ros2x*
    0cf3ee39267f        apache/couchdb:2.3.0   "tini -- /docker-ent…"   2 months ago        Up 11 hours         0.0.0.0:4369->4369/tcp, 0.0.0.0:5984->5984/tcp, 0.0.0.0:5986->5986/tcp, 0.0.0.0:5988->9100/tcp   op_couchdb
    chengc@TV2730-OptiPlex-3050:~/ros/ros2$ 

# Run the example

+ show simulator!!

> \# open a new terminal, <br>
$ docker exec -it ros2x /bin/bash

    chengc@TV2730-OptiPlex-3050:~/ros/ros2$ docker exec -it ros2x /bin/bash
    Usage:
    	TURTLEBOT model, burger|waffle|wafflepi
    	export TURTLEBOT3_MODEL=burger
    Example command:
    	export TURTLEBOT3_MODEL=burger|waffle|wafflepi
    	gazebo --verbose /opt/ros/dashing/share/gazebo_plugins/worlds/gazebo_ros_diff_drive_demo.world
    	gazebo --verbose /opt/ros/dashing/share/gazebo_plugins/worlds/gazebo_ros_tricycle_drive_demo.world
    	ros2 topic pub /demo/cmd_demo geometry_msgs/Twist '{linear: {x: 1.0}}' -1
    	sleep 2
    	ros2 topic pub /demo/cmd_demo geometry_msgs/Twist '{linear: {x: 0.0}}' -1
    root@97836d32d882:/# 

> \# run the GUI<br>
$ gazebo --verbose /opt/ros/dashing/share/gazebo_plugins/worlds/gazebo_ros_diff_drive_demo.world

### move the robot 2 seconds

+ start GUI in a terminal

> \# open second terminal<br>
$ docker exec -it ros2x /bin/bash

    chengc@TV2730-OptiPlex-3050:~/ros/ros2$ docker exec -it ros2x /bin/bash
    Usage:
    	TURTLEBOT model, burger|waffle|wafflepi
    	export TURTLEBOT3_MODEL=burger
    Example command:
    	export TURTLEBOT3_MODEL=burger|waffle|wafflepi
    	gazebo --verbose /opt/ros/dashing/share/gazebo_plugins/worlds/gazebo_ros_diff_drive_demo.world
    	gazebo --verbose /opt/ros/dashing/share/gazebo_plugins/worlds/gazebo_ros_tricycle_drive_demo.world
    	ros2 topic pub /demo/cmd_demo geometry_msgs/Twist '{linear: {x: 1.0}}' -1
    	sleep 2
    	ros2 topic pub /demo/cmd_demo geometry_msgs/Twist '{linear: {x: 0.0}}' -1
    root@97836d32d882:/# 

+ move the robot 2 seconds in new terminal

>$ ros2 topic pub /demo/cmd_demo geometry_msgs/Twist '{linear: {x: 1.0}}' -1<br>
$ sleep 2<br>
$ ros2 topic pub /demo/cmd_demo geometry_msgs/Twist '{linear: {x: 0.0}}' -1<br>


    
