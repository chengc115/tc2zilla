
xhost +
docker stop rosmn
docker rm rosmn

XAUTH=/tmp/.docker.xauth

if [ ! -f $XAUTH ]
then
    xauth_list=$(xauth nlist :0 | sed -e 's/^..../ffff/')
    if [ ! -z "$xauth_list" ]
    then
        echo $xauth_list | xauth -f $XAUTH nmerge -
    else
        touch $XAUTH
    fi
    ls -ald $XAUTH
    sudo chmod -R 0777 $XAUTH
fi


nvidia-docker run -it \
    -p 11411:11411 \
    -p 11311:11311 \
    --name=rosmn \
    --device=/dev/dri:/dev/dri  \
    --env="DISPLAY=$DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --env="XAUTHORITY=${XAUTH}" \
    --volume="$XAUTH:$XAUTH" \
    --volume=$XSOCK:$XSOCK:rw \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --volume=/home/$USER/ros:/opt/ros_ws \
    --runtime=nvidia \
    rosmn  "roscore"
    

